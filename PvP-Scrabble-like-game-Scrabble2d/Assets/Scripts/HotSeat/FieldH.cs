﻿using System;
using Mono.Data.Sqlite;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using System.Data;

//[0,0] - lower left angle
//Todo: test
public class FieldH : MonoBehaviour
{
    public enum Direction
    {
        Horizontal, Vertical, None
    }

    #region Prefabs and materials

    public TileH TileHPrefab;
    public Material StandardMaterial;
    public Material StartMaterial;
    public Material WordX2Material;
    public Material WordX3Material;
    public Material LetterX2Material;
    public Material LetterX3Material;

    #endregion Prefabs and materials

    public GameObject TimerImage;
    public Text TimerText;
    public Text Player1Text;
    public Text Player2Text;
    public GameObject EndGameCanvas;
    public UIController Controller;
    public Button SkipTurnButton;

    public UIGrid FieldGrid;
    public Direction CurrentDirection = Direction.None;
    public int CurrentTurn = 1;
    public bool isFirstTurn = true;
    public byte NumberOfRows = 15;
    public byte NumberOfColumns = 15;
    public LetterBoxH Player1;
    public LetterBoxH Player2;
    public byte CurrentPlayer = 1;
    public TileH[,] Field;
    public List<TileH> CurrentTiles;

    private int _turnsSkipped = 0;
    private SqliteConnection _dbConnection;
    private List<TileH> _wordsFound;
    private bool _timerEnabled;
    private int _timerLength;
    private float _timeRemaining;
    private List<TileH> _asterixTiles = new List<TileH>();
    private List<string> letterList;  //zeynep

    private void Start()
    {
        CurrentTiles = new List<TileH>();
        var conection = @"URI=file:" + Application.streamingAssetsPath + @"/words.db";
        _dbConnection = new SqliteConnection(conection);
        _dbConnection.Open();
        _wordsFound = new List<TileH>();
        _timerEnabled = PlayerPrefs.GetInt("TimerEnabled") == 1;
        if (_timerEnabled)
        {
            TimerImage.SetActive(true);
            _timerLength = PlayerPrefs.GetInt("Length");
            _timeRemaining = (float)_timerLength + 1;
        }
        FieldGrid.Initialize();
        var letterSize = FieldGrid.Items[0, 0].gameObject.GetComponent<RectTransform>().rect.width;
        Player1.LetterSize = new Vector2(letterSize, letterSize);
        Player2.LetterSize = new Vector2(letterSize, letterSize);
        CreateField();
        Player1Text.text = PlayerPrefs.GetString("Player1", "1. Oyuncu");
        Player2Text.text = PlayerPrefs.GetString("Player2", "2. Oyuncu");
    }

    private void Update()
    {
        if (SkipTurnButton.interactable != (CurrentTiles.Count == 0))
            SkipTurnButton.interactable = CurrentTiles.Count == 0;
        if (Input.GetKeyDown(KeyCode.A))
            EndGame(null);
        if (_timerEnabled)
        {
            _timeRemaining -= Time.deltaTime;
            var timerValue = (int)_timeRemaining - 1;
            if (timerValue < 0)
                timerValue = 0;
            TimerText.text = timerValue.ToString();
            if (_timeRemaining < 0)
                OnEndTimer();
        }
    }

    private void CreateField()
    {
        Field = new TileH[NumberOfRows, NumberOfColumns];
        for (var i = 0; i < NumberOfRows; i++)
        {
            for (var j = 0; j < NumberOfColumns; j++)
            {
                var newTile = Instantiate(TileHPrefab);
                newTile.transform.SetParent(gameObject.transform);
                newTile.Column = j;
                var render = newTile.GetComponent<Image>();
                render.material = StandardMaterial;
                newTile.Row = i;
                Field[i, j] = newTile;
                FieldGrid.AddElement(i, j, newTile.gameObject);
            }
        }
        Field[7, 7].CanDrop = true;
        Field[7, 7].GetComponent<Image>().material = StartMaterial;
        AssignMaterials();
        AssignMultipliers();
    }

    #region Field generation

    private void AssignMaterials()
    {
        Field[0, 0].GetComponent<Image>().material = WordX3Material;
        Field[0, 14].GetComponent<Image>().material = WordX3Material;
        Field[14, 0].GetComponent<Image>().material = WordX3Material;
        Field[14, 14].GetComponent<Image>().material = WordX3Material;
        Field[0, 7].GetComponent<Image>().material = WordX3Material;
        Field[14, 7].GetComponent<Image>().material = WordX3Material;
        Field[7, 0].GetComponent<Image>().material = WordX3Material;
        Field[7, 14].GetComponent<Image>().material = WordX3Material;
        for (var i = 1; i < 5; i++)
        {
            Field[i, i].GetComponent<Image>().material = WordX2Material;
            Field[i, NumberOfRows - i - 1].GetComponent<Image>().material = WordX2Material;
            Field[NumberOfRows - i - 1, i].GetComponent<Image>().material = WordX2Material;
            Field[NumberOfRows - i - 1, NumberOfRows - i - 1].GetComponent<Image>().material = WordX2Material;
        }
        Field[5, 1].GetComponent<Image>().material = LetterX3Material;
        Field[5, 5].GetComponent<Image>().material = LetterX3Material;
        Field[5, 9].GetComponent<Image>().material = LetterX3Material;
        Field[5, 13].GetComponent<Image>().material = LetterX3Material;
        Field[9, 1].GetComponent<Image>().material = LetterX3Material;
        Field[9, 5].GetComponent<Image>().material = LetterX3Material;
        Field[9, 9].GetComponent<Image>().material = LetterX3Material;
        Field[9, 13].GetComponent<Image>().material = LetterX3Material;
        Field[1, 5].GetComponent<Image>().material = LetterX3Material;
        Field[1, 9].GetComponent<Image>().material = LetterX3Material;
        Field[13, 5].GetComponent<Image>().material = LetterX3Material;
        Field[13, 9].GetComponent<Image>().material = LetterX3Material;
        Field[0, 3].GetComponent<Image>().material = LetterX2Material;
        Field[0, 11].GetComponent<Image>().material = LetterX2Material;
        Field[14, 3].GetComponent<Image>().material = LetterX2Material;
        Field[14, 11].GetComponent<Image>().material = LetterX2Material;
        Field[2, 6].GetComponent<Image>().material = LetterX2Material;
        Field[2, 8].GetComponent<Image>().material = LetterX2Material;
        Field[12, 6].GetComponent<Image>().material = LetterX2Material;
        Field[12, 8].GetComponent<Image>().material = LetterX2Material;
        Field[3, 0].GetComponent<Image>().material = LetterX2Material;
        Field[3, 7].GetComponent<Image>().material = LetterX2Material;
        Field[3, 14].GetComponent<Image>().material = LetterX2Material;
        Field[11, 0].GetComponent<Image>().material = LetterX2Material;
        Field[11, 7].GetComponent<Image>().material = LetterX2Material;
        Field[11, 14].GetComponent<Image>().material = LetterX2Material;
        Field[6, 2].GetComponent<Image>().material = LetterX2Material;
        Field[6, 6].GetComponent<Image>().material = LetterX2Material;
        Field[6, 8].GetComponent<Image>().material = LetterX2Material;
        Field[6, 12].GetComponent<Image>().material = LetterX2Material;
        Field[8, 2].GetComponent<Image>().material = LetterX2Material;
        Field[8, 6].GetComponent<Image>().material = LetterX2Material;
        Field[8, 8].GetComponent<Image>().material = LetterX2Material;
        Field[8, 12].GetComponent<Image>().material = LetterX2Material;
        Field[7, 3].GetComponent<Image>().material = LetterX2Material;
        Field[7, 11].GetComponent<Image>().material = LetterX2Material;
    }

    private void AssignMultipliers()
    {
        Field[0, 0].WordMultiplier = 3;
        Field[0, 14].WordMultiplier = 3;
        Field[14, 0].WordMultiplier = 3;
        Field[14, 14].WordMultiplier = 3;
        Field[0, 7].WordMultiplier = 3;
        Field[14, 7].WordMultiplier = 3;
        Field[7, 0].WordMultiplier = 3;
        Field[7, 14].WordMultiplier = 3;
        for (var i = 1; i < 5; i++)
        {
            Field[i, i].WordMultiplier = 2;
            Field[i, NumberOfRows - i - 1].WordMultiplier = 2;
            Field[NumberOfRows - i - 1, i].WordMultiplier = 2;
            Field[NumberOfRows - i - 1, NumberOfRows - i - 1].WordMultiplier = 2;
        }
        Field[5, 1].LetterMultiplier = 3;
        Field[5, 5].LetterMultiplier = 3;
        Field[5, 9].LetterMultiplier = 3;
        Field[5, 13].LetterMultiplier = 3;
        Field[9, 1].LetterMultiplier = 3;
        Field[9, 5].LetterMultiplier = 3;
        Field[9, 9].LetterMultiplier = 3;
        Field[9, 13].LetterMultiplier = 3;
        Field[1, 5].LetterMultiplier = 3;
        Field[1, 9].LetterMultiplier = 3;
        Field[13, 5].LetterMultiplier = 3;
        Field[13, 9].LetterMultiplier = 3;
        Field[0, 3].LetterMultiplier = 2;
        Field[0, 11].LetterMultiplier = 2;
        Field[14, 3].LetterMultiplier = 2;
        Field[14, 11].LetterMultiplier = 2;
        Field[2, 6].LetterMultiplier = 2;
        Field[2, 8].LetterMultiplier = 2;
        Field[12, 6].LetterMultiplier = 2;
        Field[12, 8].LetterMultiplier = 2;
        Field[3, 0].LetterMultiplier = 2;
        Field[3, 7].LetterMultiplier = 2;
        Field[3, 14].LetterMultiplier = 2;
        Field[11, 0].LetterMultiplier = 2;
        Field[11, 7].LetterMultiplier = 2;
        Field[11, 14].LetterMultiplier = 2;
        Field[6, 2].LetterMultiplier = 2;
        Field[6, 6].LetterMultiplier = 2;
        Field[6, 8].LetterMultiplier = 2;
        Field[6, 12].LetterMultiplier = 2;
        Field[8, 2].LetterMultiplier = 2;
        Field[8, 6].LetterMultiplier = 2;
        Field[8, 8].LetterMultiplier = 2;
        Field[8, 12].LetterMultiplier = 2;
        Field[7, 3].LetterMultiplier = 2;
        Field[7, 11].LetterMultiplier = 2;
    }

    #endregion Field generation

    private void OnEndTimer()
    {
        _timeRemaining = (float)_timerLength + 1;
        OnRemoveAll();
        OnSkipTurn();
    }

    public void OnEndTurn()
    {
        if (CurrentTiles.Count > 0)
        {
            if (CheckWords())
            {
                _turnsSkipped = 0;
                CurrentTurn++;
                var points = CountPoints();
                if (CurrentPlayer == 1)
                {
                    Player1.ChangeBox(4 - Player1.CurrentLetters.Count);
                    Player1.Score += points;
                    if (Player1.CurrentLetters.Count == 0)
                    {
                        EndGame(Player1);
                    }
                    Player1.gameObject.SetActive(false);
                    Player2.gameObject.SetActive(true);
                    CurrentTiles.Clear();
                    CurrentDirection = Direction.None;
                    CurrentPlayer = 2;
                    Controller.InvalidatePlayer(1, Player1.Score);                    
                    isFirstTurn = false;
                    WriteListAvaliableLetters(Player2.CurrentLetters);   //zeynep
                }
                else
                {
                    Player2.ChangeBox(4 - Player2.CurrentLetters.Count);
                    Player2.Score += points;
                    if (Player2.CurrentLetters.Count == 0)
                        EndGame(Player2);
                    Player1.gameObject.SetActive(true);
                    Player2.gameObject.SetActive(false);
                    CurrentDirection = Direction.None;
                    CurrentTiles.Clear();
                    CurrentPlayer = 1;
                    Controller.InvalidatePlayer(2, Player2.Score);
                    isFirstTurn = false;
                }
                if (_timerEnabled)
                    _timeRemaining = (float)_timerLength + 1;
            }
            else Controller.ShowNotExistError();
        }
        else Controller.ShowZeroTilesError();
        _wordsFound = new List<TileH>();
    }
    
    public void OnSkipTurn()
    {
        CurrentDirection = Direction.None;
        if (CurrentPlayer == 1)
        {
            Player1.gameObject.SetActive(false);           
            Player2.gameObject.SetActive(true);
            CurrentPlayer = 2;
            Controller.InvalidatePlayer(1, Player1.Score);
            WriteListAvaliableLetters(Player2.CurrentLetters);  //zeynep
        }
        else
        {
            Player1.gameObject.SetActive(true);
            Player2.gameObject.SetActive(false);
            CurrentPlayer = 1;
            Controller.InvalidatePlayer(2, Player2.Score);
        }
        if (_timerEnabled)
            _timeRemaining = (float)_timerLength + 1;
        if (++_turnsSkipped == 4)
            EndGame(null);
    }

    public void OnRemoveAll()
    {
        for (var i = CurrentTiles.Count - 1; i >= 0; i--)
        {
            CurrentTiles[i].RemoveTile();
        }
        CurrentTiles.Clear();
        CurrentDirection = Direction.None;
    }

    #region Word checking

    private bool CheckWords()
    {
        var words = CreateWords();
        _wordsFound = words;
        var word = GetWord(words[0], words[1]);
        if (_asterixTiles.Count != 0)
        {
            var index1 = word.IndexOf('_');
            var index2 = 0;
            if (_asterixTiles.Count == 2)
                index2 = word.IndexOf('_', index1 + 1);
            var variants = GetAllWordVariants(word);
            SwitchDirection();
            foreach (var variant in variants)
            {
                _asterixTiles[0].TempLetter = variant[index1].ToString();
                if (_asterixTiles.Count == 2)
                    _asterixTiles[1].TempLetter = variant[index2].ToString();
                var successful = true;
                for (var i = 3; i < words.Count; i += 2)
                {
                    word = GetWord(words[i - 1], words[i]);
                    if (!CheckWord(word))
                    {
                        successful = false;
                        break;
                    }
                }
                if (successful)
                {
                    SwitchDirection();
                    return true;
                }
            }
            SwitchDirection();
            return false;
        }
        else
        {
            var successful = CheckWord(word);
            var i = 3;
            SwitchDirection();
            while (successful && i < words.Count)
            {
                word = GetWord(words[i - 1], words[i]);
                successful = CheckWord(word);
                i += 2;
            }
            SwitchDirection();
            return successful;
        }
    }

    private List<TileH> CreateWords()
    {
        _asterixTiles.Clear();
        var res = new List<TileH>();
        if (CurrentDirection == Direction.None)
            CurrentDirection = Direction.Horizontal;
        TileH start, end;
        CreateWord(CurrentTiles[0], out start, out end);
        if (start == end)
        {
            SwitchDirection();
            CreateWord(CurrentTiles[0], out start, out end);
        }
        res.Add(start);
        res.Add(end);
        SwitchDirection();
        foreach (var tile in CurrentTiles)
        {
            CreateWord(tile, out start, out end);
            if (start != end)
            {
                res.Add(start);
                res.Add(end);
            }
        }
        if (_asterixTiles.Count == 2)
            _asterixTiles = _asterixTiles.OrderByDescending(t => t.Row).ThenBy(t => t.Column).Distinct().ToList();
        SwitchDirection();
        return res;
    }

    private void CreateWord(TileH start, out TileH wordStart, out TileH wordEnd)
    {
        if (CurrentDirection == Direction.Vertical)
        {
            var j = start.Row;
            while (j < 15 && Field[j, start.Column].HasLetter)
            {
                if (Field[j, start.Column].CurrentLetter.text.Equals("*"))
                    if (!_asterixTiles.Contains(Field[j, start.Column]))
                        _asterixTiles.Add(Field[j, start.Column]);
                j++;
            }
            wordStart = Field[j - 1, start.Column];
            j = start.Row;
            while (j >= 0 && Field[j, start.Column].HasLetter)
            {
                if (Field[j, start.Column].CurrentLetter.text.Equals("*"))
                    if (!_asterixTiles.Contains(Field[j, start.Column]))
                        _asterixTiles.Add(Field[j, start.Column]);
                j--;
            }
            wordEnd = Field[j + 1, start.Column];
        }
        else
        {
            var j = start.Column;
            while (j >= 0 && Field[start.Row, j].HasLetter)
            {
                if (Field[start.Row, j].CurrentLetter.text.Equals("*"))
                    if (!_asterixTiles.Contains(Field[start.Row, j]))
                        _asterixTiles.Add(Field[start.Row, j]);
                j--;
            }
            wordStart = Field[start.Row, j + 1];
            j = start.Column;
            while (j < 15 && Field[start.Row, j].HasLetter)
            {
                if (Field[j, start.Column].CurrentLetter.text.Equals("*"))
                    if (!_asterixTiles.Contains(Field[start.Row, j]))
                        _asterixTiles.Add(Field[start.Row, j]);
                j++;
            }
            wordEnd = Field[start.Row, j - 1];
        }
    }

    private int CountPoints()
    {
        var result = 0;
        var wordMultiplier = 1;
        var score = new int[_wordsFound.Count / 2];
        for (var i = 0; i < _wordsFound.Count; i += 2)
        {
            var tempRes = 0;
            if (_wordsFound[i].Row == _wordsFound[i + 1].Row)
                for (var j = _wordsFound[i].Column; j <= _wordsFound[i + 1].Column; j++)
                {
                    var tile = Field[_wordsFound[i].Row, j];
                    tempRes += LetterBoxH.PointsDictionary[tile.CurrentLetter.text] * tile.LetterMultiplier;
                    if (tile.LetterMultiplier != 0)
                        tile.LetterMultiplier = 1;
                    wordMultiplier *= tile.WordMultiplier;
                    tile.WordMultiplier = 1;
                }
            else
            {
                for (var j = _wordsFound[i].Row; j >= _wordsFound[i + 1].Row; j--)
                {
                    var tile = Field[j, _wordsFound[i].Column];
                    tempRes += LetterBoxH.PointsDictionary[tile.CurrentLetter.text] * tile.LetterMultiplier;
                    if (tile.LetterMultiplier != 0)
                        tile.LetterMultiplier = 1;
                    wordMultiplier *= tile.WordMultiplier;
                    tile.WordMultiplier = 1;
                }
            }
            result += tempRes;
            score[i / 2] = tempRes;
        }
        var start = 4 + _wordsFound.Count / 2;
        foreach (var i in score)
        {
            Field[start, 0].SetPoints(i * wordMultiplier);
            start--;
        }
        if (_asterixTiles.Count != 0)
            ApplyAsterixLetters();
        return result * wordMultiplier;
    }

    private void ApplyAsterixLetters()
    {
        foreach (var tile in _asterixTiles)
        {
            tile.CurrentLetter.text = tile.TempLetter;
            tile.TempLetter = null;
            tile.LetterMultiplier = 0;
            tile.WordMultiplier = 1;
        }
    }

    private void SwitchDirection()
    {
        CurrentDirection = CurrentDirection == Direction.Horizontal ? Direction.Vertical : Direction.Horizontal;
    }

    private string GetWord(TileH begin, TileH end)
    {
        if (CurrentDirection == Direction.Vertical)
        {
            var sb = new StringBuilder();
            for (var j = begin.Row; j >= end.Row; j--)
            {
                if (!String.IsNullOrEmpty(Field[j, begin.Column].TempLetter))
                    sb.Append(Field[j, begin.Column].TempLetter);
                else if (Field[j, begin.Column].CurrentLetter.text.Equals("*"))
                    sb.Append('_');
                else sb.Append(Field[j, begin.Column].CurrentLetter.text);
            }
            return sb.ToString();
            
        }
        else
        {
            var sb = new StringBuilder();
            for (var j = begin.Column; j <= end.Column; j++)
            {
                if (!String.IsNullOrEmpty(Field[begin.Row, j].TempLetter))
                    sb.Append(Field[begin.Row, j].TempLetter);
                else if (Field[begin.Row, j].CurrentLetter.text.Equals("*"))
                    sb.Append('_');
                else sb.Append(Field[begin.Row, j].CurrentLetter.text);
            }
            return sb.ToString();
            
        }
    }

    private List<string> GetAllWordVariants(string word)
    {
        var sql = "SELECT * FROM AllWords WHERE Word like \"" + word + "\"";
        var command = new SqliteCommand(sql, _dbConnection);
        var reader = command.ExecuteReader();
        if (reader.HasRows)
        {
            var res = new List<string>();
            while (reader.Read())
            {
                res.Add(reader.GetString(0));
            }
            reader.Close();
            return res;
        }
        reader.Close();
        return null;
    }

    private bool CheckWord(string word)
    {
        var sql = "SELECT count(*) FROM AllWords WHERE Word like \"" + word + "\"";
        var command = new SqliteCommand(sql, _dbConnection);
        var inp = command.ExecuteScalar();
        return Convert.ToInt32(inp) != 0;
    }

    #endregion Word checking

    private void EndGame(LetterBoxH playerOut)//Player, who ran out of letters is passed
    {
        var tempPoints = Player1.RemovePoints();
        tempPoints += Player2.RemovePoints();
        if (playerOut != null)
        {
            playerOut.Score += tempPoints;
        }
        var winner = Player1.Score > Player2.Score ? 1 : 2;
        Controller.SetWinner(winner, Player1.Score, Player2.Score, Player1Text.text, Player2Text.text);
    }

    //zeynep fonksiyonu readBoard
    public string[,] WriteWordsToConsole()
    {
        string[,] matris = new string[15,15];

        for(int row = 0; row < 15; row++)
        {
            for(int column = 0; column < 15; column++)
            {
                matris[row, column] = Field[row,column].CurrentLetter.GetComponent<Text>().text;   
                //Field[row, column].CurrentLetter.GetComponent<Text>().text = "b"; 
            }
        }

        return matris;

        //for (int k = 0; k < 15; k++)
        //{
        //    for (int j = 0; j < 15; j++)
        //    {
        //        Debug.Log("[" + k + "," + j + "]" + matris[k,j] + "  ");
        //    }            
        //}
    }


    //sdsjhfkjdsf---------------------------------------------------------------------------------------------


    private List<string> GetLetters(string[,] squares)
    {
        List<string> liste = new List<string>();

        for (int i = 0; i < 15; i++)
        {
            string kelimeSoldanSaga = "";

            for (int j = 0; j < 15; j++)
            {
                if (squares[i, j] != "")
                {
                    kelimeSoldanSaga += squares[i, j];
                }
            }
            if (kelimeSoldanSaga != "") liste.Add(kelimeSoldanSaga);
        }

        for (int j = 0; j < 15; j++)
        {
            string kelimeYukaridanAsagiya = "";

            for (int i = 0; i < 15; i++)
            {
                if (squares[i, j] != "")
                {
                    kelimeYukaridanAsagiya += squares[i, j];
                }
            }
            if (kelimeYukaridanAsagiya != "") liste.Add(kelimeYukaridanAsagiya);
        }

        return liste;
    }

    private List<List<string>> GetPowerSetOfRackLetters(List<string> setOfLetters)
    {
        List<List<string>> subsets = new List<List<string>>();
        string[] tempArray = new string[4] { setOfLetters[0], setOfLetters[1], setOfLetters[2], setOfLetters[3] };

        uint pow_set_size = (uint)Math.Pow(2, 4);
        int counter, j;

        for (counter = 0; counter < pow_set_size; counter++)
        {
            List<string> subset = new List<string>();
            for (j = 0; j < 4; j++)
            {
                if ((counter & (1 << j)) > 0)
                {
                    subset.Add(tempArray[j]);
                }
            }
            subsets.Add(subset);
        }

        subsets.RemoveAt(0);
        return subsets;
    }

    private List<string> Swap(List<string> stringsToSwap, int i, int j)
    {
        string temp;

        temp = stringsToSwap[i];
        stringsToSwap[i] = stringsToSwap[j];
        stringsToSwap[j] = temp;

        return stringsToSwap;
    }

    private List<string> Permute(List<string> elementsToPermute, List<string> permutedWordList, int l, int r)
    {
        if (l == r)
        {
            string s = string.Join("", elementsToPermute.ToArray());
            permutedWordList.Add(s);
        }
        else
        {
            for (int i = l; i <= r; i++)
            {
                elementsToPermute = Swap(elementsToPermute, l, i);
                Permute(elementsToPermute, permutedWordList, l + 1, r);
                elementsToPermute = Swap(elementsToPermute, l, i);
            }
        }

        return permutedWordList;
    }

    private List<string> GetRandomizedWords(List<string> lettersFromRack, List<string> wordsFromTable, List<string> permutedList)
    {
        List<string> randomizedWords = new List<string>();
        List<string> tempList = new List<string>();
        List<List<string>> powerSetofLettersInRack = GetPowerSetOfRackLetters(lettersFromRack);

        if (wordsFromTable.Count > 0)
        {
            foreach (List<string> ls in powerSetofLettersInRack)
            {
            
                for (int i = 0; i < wordsFromTable.Count; i++)
                {
                    tempList.AddRange(ls);
                    tempList.Add(wordsFromTable[i]);

                    randomizedWords.AddRange(Permute(tempList, permutedList, 0, tempList.Count - 1));

                    tempList.Clear();
                }
            }

        }

        else
        {
            foreach (List<string> ls in powerSetofLettersInRack)
            {
                randomizedWords.AddRange(Permute(ls, permutedList, 0, ls.Count - 1));      
            }
        }

        return randomizedWords;
    }

    private List<string> CheckRandomizedWords(List<string> wordsToCheck, List<string> checkedWords)
    {

        SqliteDataAdapter sQLiteDataAdapter;
        DataSet dataSet = new DataSet();
        DataTable dataTable = new DataTable();

        int kalan = wordsToCheck.Count % 250;
        int bölüm = wordsToCheck.Count / 250;
        int lastRange = wordsToCheck.Count - kalan;

        for (int a = 0; a < bölüm; a++)
        {
            string commandText = string.Format("SELECT Word FROM AllWords WHERE Word IN ('{0}')", string.Join("','", wordsToCheck.GetRange(a * 250, 250).ToArray()));
            sQLiteDataAdapter = new SqliteDataAdapter(commandText, _dbConnection);
            sQLiteDataAdapter.Fill(dataSet);
            dataTable = dataSet.Tables[0];

            DataRow[] dataRows = dataTable.Select();
            foreach (DataRow row in dataRows)
            {
                string st = row["Word"].ToString();
                checkedWords.Add(st);

            }
            dataSet.Clear();
            dataTable.Clear();
        }

        string commandText2 = string.Format("SELECT Word FROM AllWords WHERE Word IN ('{0}')", string.Join("','", wordsToCheck.GetRange(lastRange, kalan).ToArray()));
        sQLiteDataAdapter = new SqliteDataAdapter(commandText2, _dbConnection);
        sQLiteDataAdapter.Fill(dataSet);
        dataTable = dataSet.Tables[0];

        DataRow[] dataRows2 = dataTable.Select();
        foreach (DataRow row in dataRows2)
        {
            string st = row["Word"].ToString();
            checkedWords.Add(st);

        }
        dataSet.Clear();
        dataTable.Clear();

        return checkedWords;
    }

    private static Dictionary<char, int> PointsDictionary = new Dictionary<char, int>
       {
            {'A', 1},
            {'B', 3},
            {'C', 4},
            {'Ç', 2},
            {'D', 3},
            {'E', 1},
            {'F', 7},
            {'G', 5},
            {'Ğ', 8},
            {'H', 5},
            {'I', 2},
            {'İ', 7},
            {'J', 10},
            {'K', 1},
            {'L', 1},
            {'M', 2},
            {'N', 1},
            {'O', 2},
            {'Ö', 7},
            {'P', 5},
            {'R', 1},
            {'S', 2},
            {'Ş', 4},
            {'T', 1},
            {'U', 2},
            {'Ü', 3},
            {'V', 7},
            {'Y', 3},
            {'Z', 4}
       };

    public struct PossibleWord
    {
        public string word;
        public int value;

        public PossibleWord(string s, int i)
        {
            word = s;
            value = i;
        }
    }

    private List<PossibleWord> EvaluateWords(List<string> WordListToEvaluate)
    {
        List<PossibleWord> possibles = new List<PossibleWord>();

        for (int i = 0; i < WordListToEvaluate.Count; i++)
        {
            char[] chars = WordListToEvaluate[i].ToCharArray();
            int totalPoints = 0;

            foreach (char c in chars)
            {
                totalPoints = totalPoints + PointsDictionary[c];
            }

            PossibleWord pw = new PossibleWord(WordListToEvaluate[i], totalPoints);
            possibles.Add(pw);
        }

        PossibleWord temp;
        for (int i = 1; i <= possibles.Count; i++)
        {
            for (int j = 0; j < possibles.Count - i; j++)
            {
                if (possibles[j].value < possibles[j + 1].value)
                {
                    temp = possibles[j];
                    possibles[j] = possibles[j + 1];
                    possibles[j + 1] = temp;
                }
            }
        }

        return possibles;
    }


    public class RequestType
    {
        public List<string> letterList;
        public string[,] tahta;

        public void SetLetterList(List<string> letterList)
        {
            this.letterList = letterList;
        }

        public void SetTahta(string[,] tahta)
        {
            this.tahta = tahta;
        }
    }

    private List<PossibleWord> AIShowsUsWhatItGot(RequestType rType)
    {
        List<string> board = GetLetters(rType.tahta);
        List<string> rack = rType.letterList;
        List<string> permutedLetters = new List<string>();
        List<string> checkedPermutations = new List<string>();
        List<PossibleWord> possibleAnswers = new List<PossibleWord>();
        permutedLetters = GetRandomizedWords(rack, board, permutedLetters);
        CheckRandomizedWords(permutedLetters, checkedPermutations);
        possibleAnswers = EvaluateWords(checkedPermutations);

        return possibleAnswers;
    }

    //sdsjhfkjdsf---------------------------------------------------------------------------------------------


    //zeynep fonksiyonu
    public void WriteListAvaliableLetters(List<LetterH> playerCurrentLetters)
    {

        if (Player1.gameObject.activeSelf)
        {
            Debug.Log("Player 1 Aktif");
            return;
        }

        if (playerCurrentLetters == null || !playerCurrentLetters.Any())
        {
            Debug.Log("Liste Henüz Dolmadı");
            return;
        }

        letterList = new List<string>();

        foreach (LetterH letterH in playerCurrentLetters)
        {
            Debug.Log(" Letter = " + letterH.LetterText.text);
            letterList.Add(letterH.LetterText.text);
        }

        string[,] tahta = new string[15,15];
        tahta = WriteWordsToConsole();

        RequestType requestType = new RequestType();
        requestType.SetLetterList(letterList);
        requestType.SetTahta(tahta);

        List<PossibleWord> answerList = new List<PossibleWord>();
        answerList = AIShowsUsWhatItGot(requestType);

        if ((answerList.Count > 0))
        {
            Debug.Log("cevap => " + answerList[0].word + " = " + answerList[0].value.ToString());
        }
        //else if (answerList.Count >= 2)
        //{
        //    Debug.Log("öncelikli cevap => " + answerList[0].word + " = " + answerList[0].value.ToString());
        //    Debug.Log("ikincil cevap => " + answerList[1].word + " = " + answerList[1].value.ToString());
        //}
        else
        {
            Debug.Log("cevap yok!");
        }
    }

    //zeynep fonksiyonu


}
